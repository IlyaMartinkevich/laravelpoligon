<?php

namespace Database\Factories\Trello;

use App\Models\Trello\List_;
use Illuminate\Database\Eloquent\Factories\Factory;

class List_Factory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = List_::class;

    public function definition()
    {
        return [
            "name" => "List: " . $this->faker->name(),
            "desk_id" => rand(1, 10)
        ];
    }
}
