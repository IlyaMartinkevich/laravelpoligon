<?php

namespace App\Http\Resources\Trello;

use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'list_id' => $this->list_id,
            'created_at'=>$this->created_at,
            'tasks'=>TaskResource::collection($this->tasks),
        ];
    }
}
