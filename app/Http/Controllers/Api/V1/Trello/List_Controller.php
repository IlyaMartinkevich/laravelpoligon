<?php

namespace App\Http\Controllers\Api\V1\Trello;

use App\Http\Controllers\Controller;
use App\Http\Resources\Trello\List_Resource;
use App\Models\Trello\List_;
use Illuminate\Http\Request;

class List_Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return List_Resource::collection(List_::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(List_ $list)
    {
        return  new List_Resource($list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(List_ $list)
    {
        $list->delete();
        return response()->json([
            'status' => 'OK',
            'message' => 'List deleted. id:'.$list->id
        ]);
    }
}
