<?php

namespace Database\Factories\Trello;

use App\Models\Trello\Card;
use Illuminate\Database\Eloquent\Factories\Factory;

class CardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Card::class;

    public function definition()
    {
        return [
            "name" => "Card: " . $this->faker->name(),
            "list_id" => rand(1, 200)
        ];
    }
}
