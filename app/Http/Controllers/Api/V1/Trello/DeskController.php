<?php

namespace App\Http\Controllers\Api\V1\Trello;

use App\Http\Controllers\Controller;
use App\Http\Requests\Trello\DeskStoreRequest;
use App\Http\Resources\Trello\DeskResource;
use App\Models\Trello\Desk;
use Illuminate\Http\Request;

class DeskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return Desk::all();
       return DeskResource::collection(Desk::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DeskResource
     */
    public function store(DeskStoreRequest $request)
    {
        $createdDesk = Desk::create($request->validated());
        return  new DeskResource($createdDesk);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Desk $desk)
    {
      // return  Desk::find($id);
       return  new DeskResource($desk);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeskStoreRequest $request, Desk $desk)
    {
        $desk->update($request->validated());
        return  new DeskResource($desk);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( Desk $desk)
    {
        $desk->delete();
        return response()->json([
            'status' => 'OK',
            'message' => 'Desc deleted. id:'.$desk->id,
        ]);
    }
}
