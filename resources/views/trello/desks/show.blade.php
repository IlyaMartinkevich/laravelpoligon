@extends('layouts.app')

@section('content')
    <h3><a href="{{route('desk_web.create')}}">Create new desk</a></h3>
    <h3><a href="{{route('desk_web.index')}}">Show All Desks</a></h3>


    <h1>Id: {{$desk->id}}</h1>
    <h1>Name: {{$desk->name}}</h1>
    <h1>Description: {{$desk->description}}</h1>

    <h1>Lists</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
        </tr>
        </thead>
        @foreach($desk->lists as $list)
            <tr>
                <th scope="row">{{$list->id}}</th>
                <td><a href="{{ route('list_web.show',$list->id)  }}">{{$list->name}}</a></td>
                <td>
                    <a href="{{ route('list_web.edit',$list->id)}}">
                        <button class="btn btn-warning" type="submit">Edit</button>
                    </a>
                </td>
                <td>
                    <form method="POST" action="{{route('list_web.destroy',$list->id)}}">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
