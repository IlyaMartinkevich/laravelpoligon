<?php

namespace Database\Factories\Trello;

use App\Models\Trello\Desk;
use Illuminate\Database\Eloquent\Factories\Factory;

class DeskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Desk::class;

    public function definition()
    {
        return [
            "name" => "Desk: " . $this->faker->name(),
            "description" => $this->faker->paragraph(2),
        ];
    }
}
