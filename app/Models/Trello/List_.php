<?php

namespace App\Models\Trello;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class List_ extends Model
{
    use HasFactory;

    protected $table = 'lists';
    protected $fillable = ['name','desk_id'];
    public function cards()
    {
        return $this->hasMany(Card::class,'list_id');
    }

}
