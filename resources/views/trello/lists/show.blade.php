@extends('layouts.app')

@section('content')
    <h3><a href="{{route('desk_web.create')}}">Create new desk</a></h3>
    <h3><a href="{{route('desk_web.index')}}">Show All Desks</a></h3>


    <h1>Id: {{$list->id}}</h1>
    <h1>Name: {{$list->name}}</h1>
    <h1>Description: {{$list->description}}</h1>

    <h1>Cards</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
        </tr>
        </thead>

            @foreach($list->cards as $card)
            <tr>
                <th scope="row">{{$card->id}}</th>
                <td>{{$card->name}}</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            @endforeach
    </table>
@endsection
