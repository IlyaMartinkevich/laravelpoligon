@extends('layouts.app')

@section('content')

    <h3><a href="{{route('desk_web.create')}}">Create new desk</a></h3>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($desks as $desk)

            <tr>
                <th scope="row">{{$desk->id}}</th>
                <td><a href="{{ route('desk_web.show',$desk->id)  }}">{{$desk->name}}</a></td>
                <td>{{$desk->description}}</td>
                <td>
                    <a href="{{ route('desk_web.edit',$desk->id)}}">
                        <button class="btn btn-warning" type="submit">Edit</button>
                    </a>
                </td>

                <td>
                    <form method="POST" action="{{route('desk_web.destroy',$desk->id)}}">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
    {!! $desks->links() !!}
@endsection
