<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
class BlogPostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(6);
        return [
            'category_id' => rand(1,10),
            'user_id' => rand(1,10),
            'slug' =>  Str::slug($title),
            'title' => $title,
            'excerpt' => $this->faker->paragraph(3,6),
            'content_raw' => $this->faker->paragraph(3,6),
            'content_html' => $this->faker->paragraph(3,6),
            'is_published' => array_rand([true,false]),
            'published_at' => $this->faker->dateTimeBetween('-2 months','-2 days') ,
            'created_at' => $this->faker->dateTimeBetween('-2 months','-2 days') ,
            'updated_at' => $this->faker->dateTimeBetween('-2 months','-2 days') ,
        ];
    }
}
