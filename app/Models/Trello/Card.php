<?php

namespace App\Models\Trello;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;
    protected $fillable = ['name','list_id'];
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
