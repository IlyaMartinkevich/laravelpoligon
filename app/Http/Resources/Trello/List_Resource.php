<?php

namespace App\Http\Resources\Trello;

use Illuminate\Http\Resources\Json\JsonResource;

class List_Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'desk_id' => $this->desk_id,
            'created_at' => $this->created_at,
            'cards' => CardResource::collection($this->cards),
        ];
    }
}
