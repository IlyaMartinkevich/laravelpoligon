@extends('layouts.app')

@section('content')
    <div class="container">
        @if($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&#10008;</span>
                        </button>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        @endif
        <form action="{{route('list_web.update', $list->id)}}" method="POST">
            @method('PUT')
            @csrf
            <input type="hidden" name="desk_id" value="{{$list->desk_id}}">
            <div class="form-group">
                <input type="text" name="name" value="{{old('name', $list->name)}}">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Save</button>
                <a class="btn btn-primary" href="{{route('desk_web.show',$list->desk_id)}}">Back</a>

            </div>
        </form>
    </div>
@endsection
