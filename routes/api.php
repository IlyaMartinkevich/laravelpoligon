<?php

use App\Http\Controllers\Api\V1\Trello\DeskController;
use App\Http\Controllers\Api\V1\Trello\List_Controller;
use App\Http\Controllers\Api\V1\Trello\CardController;
use App\Http\Controllers\Api\V1\Trello\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    'desk' => DeskController::class,
    'list' => List_Controller::class,
    'card' => CardController::class,
    'task' => TaskController::class
]);

