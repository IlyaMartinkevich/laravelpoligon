<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BlogCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(3);
        return [
            'slug' => Str::slug ($title),
            'parent_id' => rand(1,4),
            'title' =>$title,
            'description' => $this->faker->paragraph(3,true),
        ];
    }
}
