<?php

namespace App\Http\Controllers\Trello;

use App\Http\Controllers\Controller;
use App\Http\Requests\Trello\DeskStoreRequest;
use App\Models\Trello\Desk;
use App\Models\Trello\List_;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
class DeskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $desks = Desk::paginate(5,['id','name','description']);
        return view('trello.desks.index',['desks'=>$desks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desk = new Desk();
        return view('trello.desks.create',['item'=>$desk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeskStoreRequest $request)
    {
        Desk::create($request->validated());
        return redirect()->route('desk_web.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Trello\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $desk = Desk::with('lists')->findOrFail($id);
        return view('trello.desks.show',['desk'=>$desk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Trello\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function edit( $id,Request $request)
    {
        $desk = Desk::findOrFail($id);
        return view('trello.desks.edit',['desk'=>$desk]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Trello\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function update(DeskStoreRequest $request, $id)
    {
        Desk::findOrFail($id)->update($request->validated());
        return redirect()->route('desk_web.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Trello\Desk  $desk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Desk::destroy($id);
        return redirect()->route('desk_web.index');
    }
}
