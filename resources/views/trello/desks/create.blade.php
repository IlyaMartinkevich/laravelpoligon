@extends('layouts.app')


@section('content')
    <div class="container">
        @if($errors->any())
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&#10008;</span>
                        </button>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        @endif
        <form action="{{route('desk_web.store')}}" method="POST">
            @csrf
            <div class="form-group">
                <input type="text" name="name" value="{{old('name', $item->name)}}">
            </div>
            <div class="form-group">
                <textarea name="description" id="" cols="30" rows="10">{{old('description', $item->description)}}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Save</button>
            </div>
        </form>
        <a class="btn btn-primary" href="{{route('desk_web.index')}}">Back</a>
    </div>
@endsection
