<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RestTestController;
use App\Http\Controllers\Blog\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('blog/posts', PostController::class);
Route::resource('/trello/desk_web' , App\Http\Controllers\Trello\DeskController::class);
Route::resource('/trello/list_web' , App\Http\Controllers\Trello\ListController::class);

Route::resource('test', RestTestController::class);
Route::resource('/blog/admin/categories', \App\Http\Controllers\Blog\Admin\CategoryController::class);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
