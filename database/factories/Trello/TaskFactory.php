<?php

namespace Database\Factories\Trello;

use App\Models\Trello\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Task::class;

    public function definition()
    {
        return [
            "name" => "Task: " . $this->faker->name(),
            "card_id" => rand(1, 400)
        ];
    }
}
