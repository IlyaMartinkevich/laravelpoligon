<?php

namespace App\Models\Trello;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desk extends Model
{
    use HasFactory;

    protected $fillable = ['name','description'];
    public function lists()
    {
        return $this->hasMany(List_::class);
    }





}
