@csrf
<div class="container">
    @if($errors->any())
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&#10008;</span>
                    </button>
                    {{$errors->first()}}
                </div>
            </div>
        </div>
    @endif
    @if(session('success'))
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&#10008;</span>
                    </button>
                    {{ session()->get('success')}}
                </div>
            </div>
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="form-group">
                <lable for="title">Заголовок</lable>
                <input id="title" type="text" name="title" class="form-control"
                       value="{{old('title', $item->title)}}">
            </div>
            <div class="form-group">

                <lable for="slug">Идентификатор</lable>
                <input id="slug" type="text" name="slug" class="form-control"
                       value="{{old('slug', $item->slug)}}">
            </div>
            <div class="form-group">

                <lable for="description">Описание</lable>
                <textarea class="form-control" name="description" id="description"
                          rows="8">{{old('description', $item->description)}}</textarea>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card-body">
                <button class="btn btn-primary" type="submit">Сохранить</button>
            </div>
            <div class="card-body">
                <a class="btn btn-primary" href="{{route('categories.index')}}">Назад</a>
            </div>

        </div>

    </div>
</div>
