<?php

use App\Http\Controllers\Api\V2\Trello\DeskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::resources([
    'desksnew' => DeskController::class
]);

