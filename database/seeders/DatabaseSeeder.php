<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(10)->create();
         \App\Models\BlogCategory::factory(10)->create();
        \App\Models\BlogPost::factory(10)->create();

        \App\Models\Trello\Desk::factory(10)->create();
        \App\Models\Trello\List_::factory(200)->create();
        \App\Models\Trello\Card::factory(400)->create();
        \App\Models\Trello\Task::factory(1000)->create();
    }
}
