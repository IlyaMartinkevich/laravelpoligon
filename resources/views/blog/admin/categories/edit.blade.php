@extends('layouts.app')
@section('content')
    <form method="POST" action="{{route('categories.update',$item->id)}}">
        @method('PUT')
        @include('blog.admin.categories.include.main')
    </form>
@endsection
