<?php

namespace App\Repositories\Blog;

use Illuminate\Database\Eloquent\Model;

abstract class CoreRepositories
{
    /**
     * @var Model
     */
    protected $model;

        public function __construct()
        {
            $this->model = app($this->getModelClass());
        }

    abstract protected function getModelClass();
    protected function startCondition(){
        return clone $this->model;
    }
}
